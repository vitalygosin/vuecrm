const ERORR_CODES = {
    EMAIL_NOT_FOUND: 'EMAIL NOT FOUND',
    INVALID_PASSWORD: 'INVALID PASSWORD',
    EMAIL_EXISTS: 'EMAIL EXISTS'
}
export function error(code){
return ERORR_CODES[code] ? ERORR_CODES[code] : 'unknown error'
}