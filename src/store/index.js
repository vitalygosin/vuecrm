import { createStore, createLogger } from 'vuex'
import auth from './modules/auth'

const plugins =[]
if(process.env.NODE_ENV === 'development'){
  plugins.push(createLogger())
}

export default createStore({
  plugins,
  state() {
    return {
      message: null
    }
  },
  getters: {
  },
  mutations: {
    setMessage(state, message){
      state.message = message
    },
    clearMessage(state){
      state.message = null
    }
  },
  actions: {
  },
  modules: {
    auth
  }
})
