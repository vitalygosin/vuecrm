import axios from "axios";
import {error} from "@/utils/error";
const TOKEN_KEY = 'jwt-token'
export default {
    //namespaced: true,
    state() {
        return {
            token: localStorage.getItem(TOKEN_KEY)
        }
    },
    getters: {
        token(state) {
            return state.token
        },
        isAuthenticated(state) {
            return !!state.token
        }
    },

    actions: {
            async login({dispatch, commit}, {email, password}) {
                try {
                    const url = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.VUE_APP_FB_KEY}`
                    const {data} = await axios.post(url, {email, password, returnSecureToken: true} )
                    commit('setToken', data.idToken)
                }
                catch (e) {
                    console.log(error(e.response.data.error.message))
                    throw e.response.data.error.message
                }
            },
        async register({dispatch, commit}, {email, password}) {
            try {
                const url = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${process.env.VUE_APP_FB_KEY}`
                const {data} = await axios.post(url, {email, password, returnSecureToken: true} )
                commit('setToken', data.idToken)
            }
            catch (e) {
                console.log(error(e.response.data.error.message))
                throw e.response.data.error.message
            }
        },

        logout(state) {
            state.token = null
            localStorage.removeItem('jwt-token')
        }
        },
        mutations: {
            setToken(state, token) {
                state.token = token
                localStorage.setItem('jwt-token', token)
            },

        }

}